# README
TODO:
* put back csrf protection
* validation for trigrams (upper case, 3 chars)
* test perf
* mettre en place de la pagination
* remove files
* make controller less fat
* add rspec tests
## Run
```
docker-compose build
docker-compose run web rake db:create
docker-compose run web rake db:migrate
docker-compose up
```
https://docs.docker.com/compose/rails/

## test
```
docker build -t test -f Dockerfile_python .
docker run -v $(pwd):/app --network host test python /app/test.py
```
