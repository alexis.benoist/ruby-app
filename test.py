import itertools
import string

import requests


def url(path):
    return f"http://localhost:3000/api/v1/{path}"


def get_all_existing_trigrams():
    rv = requests.get(url('names'))
    assert rv.status_code == 200
    trigrams = [item['trigram'] for item in rv.json()['names']]
    return trigrams


def make_available_trigram(existing_trigrams):
    for candidate_combination in itertools.combinations(string.ascii_lowercase, 3):
        candidate_trigram = ''.join(candidate_combination).upper()
        if candidate_trigram not in existing_trigrams:
            return candidate_trigram


def create_trigram(trigram):
    rv = requests.post(url('names'), {'trigram': trigram})
    assert rv.status_code == 200
    if rv.text == 'OK':
        return trigram
    return rv.text


def test_scenario():
    existing_trigrams = get_all_existing_trigrams()
    available_trigram = make_available_trigram(existing_trigrams)
    trigram_no_conflict = create_trigram(available_trigram)
    assert trigram_no_conflict == available_trigram
    trigram_conflict = create_trigram(available_trigram)
    assert trigram_conflict != available_trigram
    assert trigram_conflict not in existing_trigrams
    trigram_conflict_2 = create_trigram(available_trigram)
    assert trigram_conflict_2 != trigram_conflict


if __name__ == '__main__':
    test_scenario()
