class NamesController < ApplicationController
  def index
    @names = Name.all
    render json: {names: @names}
  end

  def create
    requested_trigram = name_params
    trigram = Name.create(requested_trigram)
    response = get_create_response(requested_trigram, trigram)
    render html: response
  end

  private

  def get_create_response(requested_trigram, trigram)
    if requested_trigram == trigram
      'OK'
    else
      trigram
    end
  end
end

def name_params
  # all caps check
  # check 3 letters
  # Check behavior if extra args supplied
  params.require(:trigram)
end