class Name < ApplicationRecord
  def self.create(requested_trigram)
    trigram = Name.get_available_trigram(requested_trigram)
    name = Name.new({trigram: trigram})
    name.save
    name.trigram
  end

  def self.get_available_trigram(requested_trigram)
    @trigram_is_available = !Name.exists?(trigram: requested_trigram)
    if @trigram_is_available
      requested_trigram
    else
      get_next_trigram
    end
  end


  def self.get_next_trigram
    ActiveRecord::Base.connection.execute(%Q{
        with alphabet as (
      select * from (values
        ('A'), ('B'), ('C'), ('D'), ('E'), ('F'), ('G'), ('H'),
        ('I'), ('J'), ('K'), ('L'), ('M'), ('N'), ('O'), ('P'),
        ('Q'), ('R'), ('S'), ('T'), ('U'), ('V'), ('W'), ('X'),
        ('Y'), ('Z')) c(name)
    )
    select candidate from (
      select c1.name || c2.name || c3.name as candidate
      from alphabet as c1
      cross join alphabet as c2
      cross join alphabet as c3
    ) candidate2
    where candidate not in (select trigram from names)
    order by candidate
    limit 1;
    }).field_values('candidate').first
  end
end
